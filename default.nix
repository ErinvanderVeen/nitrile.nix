{ pkgs ? import <nixpkgs> { } }:
{
  base-clm = pkgs.callPackage ./packages/base-clm.nix { };
  base-code-generator = pkgs.callPackage ./packages/base-code-generator.nix { };
  base-compiler = pkgs.callPackage ./packages/base-compiler.nix { };
  base-stdenv = pkgs.callPackage ./packages/base-stdenv.nix { };
  nitrile-bin = pkgs.callPackage ./packages/nitrile-bin.nix { };
}
