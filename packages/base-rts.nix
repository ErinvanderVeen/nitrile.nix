{ stdenv, lib }:
stdenv.mkDerivation rec {
  name = "base-rts-${version}";
  version = "1.0.3";

  src = builtins.fetchTarball {
    url = "https://gitlab.com/clean-nc/base/rts/-/package_files/24749178/download";
    sha256 = "0000000000000000000000000000000000000000000000000000";
  };

  dontConfigure = true;
  dontBuild = true;

  installPhase = ''
    mkdir -p $out
    mv * $out
  '';

  meta = with lib; {
    homepage = https://gitlab.com/clean-nc/base/rts;
    description = "The Clean run time system, forked for Isocyanoclean";
    license = licenses.agpl3Only;
    platforms = platforms.linux;
    maintainers = with maintainers; [ erin ];
  };
}

