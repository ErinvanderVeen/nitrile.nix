{ stdenv, lib }:
stdenv.mkDerivation rec {
  name = "base-stdenv-${version}";
  version = "1.0.4";

  src = builtins.fetchTarball {
    url = "https://gitlab.com/clean-nc/base/stdenv/-/package_files/24693381/download";
    sha256 = "0qpggpkgpm7hlggcv59r6aryi984yvlv3x4npadzj5nw3q5mw5vj";
  };

  dontConfigure = true;
  dontBuild = true;

  installPhase = ''
    mkdir -p $out
    mv * $out
  '';

  meta = with lib; {
    homepage = https://gitlab.com/clean-nc/base/stdenv;
    description = "The StdEnv library, forked for Isocyanoclean";
    license = licenses.agpl3Only;
    platforms = platforms.linux;
    maintainers = with maintainers; [ erin ];
  };
}

