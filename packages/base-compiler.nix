{ stdenv, lib }:
stdenv.mkDerivation rec {
  name = "base-compiler-${version}";
  version = "1.1.0";

  src = builtins.fetchTarball {
    url = "https://gitlab.com/clean-nc/base/compiler/-/package_files/27944439/download";
    sha256 = "1v0kw38fp5qhyhfaid4lqbzfc6rrrsr6hscsw0fnzs9qc16nn6n3";
  };

  dontConfigure = true;
  dontBuild = true;

  installPhase = ''
    mkdir -p $out
    mv * $out
  '';

  fixupPhase = ''
    patchelf \
      --set-interpreter "$(cat $NIX_CC/nix-support/dynamic-linker)" \
      $out/exe/cocl
  '';

  meta = with lib; {
    homepage = https://gitlab.com/clean-nc/base/compiler;
    description = "The Clean compiler, forked for Isocyanoclean";
    license = licenses.agpl3Only;
    platforms = platforms.linux;
    maintainers = with maintainers; [ erin ];
  };
}

