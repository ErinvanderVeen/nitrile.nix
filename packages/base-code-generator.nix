{ stdenv, lib }:
stdenv.mkDerivation rec {
  name = "base-code-generator-${version}";
  version = "1.0.3";

  src = builtins.fetchTarball {
    url = "https://gitlab.com/clean-nc/base/code-generator/-/package_files/24691573/download";
    sha256 = "03mbbpy30p84ivddysc6wzjxclpg9s6qakj7svzjv46k6xy0hpaw";
  };

  dontConfigure = true;
  dontBuild = true;

  installPhase = ''
    mkdir -p $out
    mv * $out
  '';

  fixupPhase = ''
    patchelf \
      --set-interpreter "$(cat $NIX_CC/nix-support/dynamic-linker)" \
      $out/exe/cg
  '';

  meta = with lib; {
    homepage = https://gitlab.com/clean-nc/base/compiler;
    description = "The Clean code generator, forked for Isocyanoclean";
    license = licenses.agpl3Only;
    platforms = platforms.linux;
    maintainers = with maintainers; [ erin ];
  };
}
