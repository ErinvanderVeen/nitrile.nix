{ stdenv, lib }:
stdenv.mkDerivation rec {
  name = "base-clm-${version}";
  version = "1.3.4";

  src = builtins.fetchTarball {
    url = "https://gitlab.com/clean-nc/base/clm/-/package_files/27940839/download";
    sha256 = "1s6vv1clfcngd5159pp0plm8qm61lhxb100ld7xibr5jdfk0b7q7";
  };

  dontConfigure = true;
  dontBuild = true;

  installPhase = ''
    mkdir -p $out
    mv * $out
  '';

  fixupPhase = ''
    patchelf \
      --set-interpreter "$(cat $NIX_CC/nix-support/dynamic-linker)" \
      $out/bin/clm
  '';

  meta = with lib; {
    homepage = https://gitlab.com/clean-nc/base/clm;
    description = "The Clean make tool, forked for Isocyanoclean";
    license = licenses.agpl3Only;
    platforms = platforms.linux;
    maintainers = with maintainers; [ erin ];
  };
}

