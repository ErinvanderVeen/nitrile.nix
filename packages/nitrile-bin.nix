{ stdenv, lib, z3 }:
stdenv.mkDerivation rec {
  name = "nitrile-bin-${version}";
  version = "0.2.16";

  src = builtins.fetchTarball {
    url = "https://gitlab.com/clean-nc/nitrile/-/package_files/27554731/download";
    sha256 = "1dbnyv8ivxir2qs2swhrdfi1l597lmnxiq1dad03kcbkw6xckn6x";
  };
  sourceRoot = ".";

  dontConfigure = true;
  dontBuild = true;

  installPhase = ''
    mkdir -p $out/bin
    mv source/bin/nitrile $out/bin/
  '';

  postFixup =
    let
      libPath = lib.makeLibraryPath [
        z3 # libz3.so
      ];
    in
    ''
      patchelf \
        --replace-needed libz3.so.4 libz3.so \
        --set-interpreter "$(cat $NIX_CC/nix-support/dynamic-linker)" \
        $out/bin/nitrile

      patchelf \
        --set-rpath "${libPath}" \
        $out/bin/nitrile
    '';

  meta = with lib; {
    homepage = https://clean-nc.gitlab.io/nitrile/;
    description = "a package manager for Isocyanoclean, a fork of the functional programming language Clean";
    license = licenses.agpl3Only;
    platforms = platforms.linux;
    maintainers = with maintainers; [ erin ];
  };
}
